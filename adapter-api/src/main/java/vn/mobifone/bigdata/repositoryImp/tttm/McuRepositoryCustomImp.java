package vn.mobifone.bigdata.repositoryImp.tttm;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;
import java.util.UUID;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import vn.mobifone.bigdata.entity.respone.ApiResponse;
import vn.mobifone.bigdata.entity.respone.DBResponse;
import vn.mobifone.bigdata.entity.things.TTTMThing;
import vn.mobifone.bigdata.repository.tttm.tttmrepo.McuRepositoryCustom;
import vn.mobifone.bigdata.util.RespUtil;

@Repository
public class McuRepositoryCustomImp implements McuRepositoryCustom {

    private static final Logger logger = LoggerFactory.getLogger(McuRepositoryCustomImp.class);

    @Autowired
    @Qualifier("tttmDataSource")
    DataSource dataSource;

    @Override
    public DBResponse mcuMapperInsert(TTTMThing tttmThing) throws Exception {
        DBResponse dbResponse = new DBResponse();
        String sqlCall ="call \"MCU_MAPPER_INSERT\"( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try(Connection conn = dataSource.getConnection();
            CallableStatement mapperInsertCallableStatement = conn.prepareCall(sqlCall);
        ){
            mapperInsertCallableStatement.setObject(1, UUID.fromString(tttmThing.getThingId()));
            mapperInsertCallableStatement.setObject(2, UUID.fromString(tttmThing.getThingKey()));
            mapperInsertCallableStatement.setString(3, tttmThing.getMcuId());
            mapperInsertCallableStatement.setString(4, tttmThing.getvCode());
            mapperInsertCallableStatement.setString(5, tttmThing.getName());
            mapperInsertCallableStatement.setString(6, tttmThing.getAddress());
            mapperInsertCallableStatement.setString(7, tttmThing.getLatitude());
            mapperInsertCallableStatement.setString(8, tttmThing.getLongitude());
            mapperInsertCallableStatement.setString(9, tttmThing.getSimNumber());
            mapperInsertCallableStatement.setString(10, tttmThing.getSimSerial());
            mapperInsertCallableStatement.setString(11, "");
            mapperInsertCallableStatement.setString(12, "");
            mapperInsertCallableStatement.setString(13, "");

            mapperInsertCallableStatement.registerOutParameter(11, Types.VARCHAR);
            mapperInsertCallableStatement.registerOutParameter(12,Types.VARCHAR);
            mapperInsertCallableStatement.registerOutParameter(13,Types.VARCHAR);

            mapperInsertCallableStatement.execute();
            String x = mapperInsertCallableStatement.getString(13);
            dbResponse.setErrorId(mapperInsertCallableStatement.getString(11));
            dbResponse.setErrorCode(mapperInsertCallableStatement.getString(12));
            dbResponse.setErrorDesc(mapperInsertCallableStatement.getString(13));

        }catch (Exception e){
            logger.error(e.getMessage());
            throw e;
        }
        return dbResponse;
    }

    @Override
    public DBResponse mcuMapperRemove(TTTMThing tttmThing) throws Exception {
        DBResponse dbResponse = new DBResponse();
        String sqlCall ="call \"MCU_MAPPER_REMOVE\"( ?, ?, ?, ?, ?, ?)";
        try(Connection conn = dataSource.getConnection();
            CallableStatement mapperRemoveCallableStatement = conn.prepareCall(sqlCall);
            ){
            mapperRemoveCallableStatement.setString(1, tttmThing.getMcuId());
            mapperRemoveCallableStatement.setObject(2, UUID.fromString(tttmThing.getThingId()));
            mapperRemoveCallableStatement.setObject(3, UUID.fromString(tttmThing.getThingKey()));
            mapperRemoveCallableStatement.setString(4, "");
            mapperRemoveCallableStatement.setString(5, "");
            mapperRemoveCallableStatement.setString(6, "");
            mapperRemoveCallableStatement.registerOutParameter(4, Types.VARCHAR);
            mapperRemoveCallableStatement.registerOutParameter(5,Types.VARCHAR);
            mapperRemoveCallableStatement.registerOutParameter(6,Types.VARCHAR);

            mapperRemoveCallableStatement.execute();

            dbResponse.setErrorId(mapperRemoveCallableStatement.getString(4));
            dbResponse.setErrorCode(mapperRemoveCallableStatement.getString(5));
            dbResponse.setErrorDesc(mapperRemoveCallableStatement.getString(6));

        }catch (Exception e){
            logger.error(e.getMessage());
            e.printStackTrace();
            throw e;
        }
        return dbResponse;
    }

	@Override
	public ApiResponse mcuCollectLog(TTTMThing tttmThing) throws Exception {
		// TODO Auto-generated method stub
		ApiResponse apiResponse = new ApiResponse();
		String sqlCall ="call \"MCU_LOG_COLLECT\"( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try(Connection conn = dataSource.getConnection();
            CallableStatement upperFunc = conn.prepareCall(sqlCall);
            ){
        	upperFunc.setString(1, tttmThing.getMcuId() );
            upperFunc.setString(2, tttmThing.getvCode() );
            upperFunc.setString(3, tttmThing.getTelNumber() );
            upperFunc.setString(4, tttmThing.getVolume() );
            upperFunc.setString(5, tttmThing.getIp() );
            upperFunc.setString(6, tttmThing.getFirmwareVersion() );
            upperFunc.setString(7, tttmThing.getFmVolume() );
            upperFunc.setString(8, tttmThing.getFmAuto() );
            upperFunc.setString(9, tttmThing.getTxType() );
            upperFunc.setString(10, tttmThing.getGroupIdList() );
            upperFunc.setString(11, tttmThing.getCfgCameraList() );
            upperFunc.setString(12, tttmThing.getCfgSensorList() );
            upperFunc.setString(13, tttmThing.getCfgEventList() );
            upperFunc.setString(14, "");
            upperFunc.setString(15, "");
            upperFunc.setString(16, "");

            upperFunc.registerOutParameter(14,Types.VARCHAR);
            upperFunc.registerOutParameter(15,Types.VARCHAR);
            upperFunc.registerOutParameter(16,Types.VARCHAR);

            upperFunc.execute();

            String errorId = upperFunc.getString(14);
            if (errorId.equals(RespUtil.MCU_COLLECT_LOG_CODE_SUCCESS)) {
                apiResponse.setStatus(RespUtil.RESPONSE_CODE_OK);
                apiResponse.setCode(RespUtil.MCU_COLLECT_LOG_CODE_SUCCESS);
                apiResponse.setMessage(RespUtil.MCU_COLLECT_LOG_MESSAGE_SUCCESS);
            } else {
                apiResponse.setStatus(RespUtil.RESPONSE_CODE_NOK);
                apiResponse.setCode(RespUtil.MCU_COLLECT_LOG_FAILED_CODE);
                apiResponse.setMessage(RespUtil.MCU_COLLECT_LOG_FAILED_MESSAGE);
            }

        }catch (Exception e){
            logger.error(e.getMessage());
            throw e;
        }
        return apiResponse;
	}

}
