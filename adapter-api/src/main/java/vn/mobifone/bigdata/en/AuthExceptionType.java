package vn.mobifone.bigdata.en;

import java.util.HashMap;
import java.util.Map;

public enum AuthExceptionType {

    UNKNOWN("UNKNOW"), GET_TOKEN_ERROR("GET_TOKEN_ERROR");

    private String value;

    private AuthExceptionType(String t) {
        value = t;
    }

    public String getValue() {
        return value;
    }

    private static final Map<String, AuthExceptionType> stringToTypeMap = new HashMap<>();

    static {
        for (AuthExceptionType item : AuthExceptionType.values()) {
            stringToTypeMap.put(item.getValue(), item);
        }
    }

    public static AuthExceptionType fromString(int i) {

        AuthExceptionType type = stringToTypeMap.get(i);
        if (type == null)
            return AuthExceptionType.UNKNOWN;
        return type;
    }
}
