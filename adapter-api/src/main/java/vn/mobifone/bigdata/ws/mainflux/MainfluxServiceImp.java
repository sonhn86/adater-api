package vn.mobifone.bigdata.ws.mainflux;

import com.fasterxml.jackson.core.JsonProcessingException;
import vn.mobifone.bigdata.en.HttpMethod;
import vn.mobifone.bigdata.entity.respone.mainflux.MainfluxResponse;
import vn.mobifone.bigdata.ws.base.WSHandle;
import vn.mobifone.bigdata.ws.base.WSLogger;
import vn.mobifone.bigdata.ws.base.WSUtil;

import java.util.Map;

public class MainfluxServiceImp  extends MainfluxService{

    public MainfluxServiceImp(String url, String jsonBody, Map<String, String> headerParams) {
        super(url, jsonBody, headerParams);
    }

    public MainfluxServiceImp(String url, String jsonBody, Map<String, String> headerParams, HttpMethod httpMethod) {
        super(url, jsonBody, headerParams, httpMethod);
    }

    public MainfluxResponse executeSevice(){
        WSLogger<MainfluxService> wsLogger = new WSLogger<MainfluxService>();
        WSHandle<MainfluxService>[] wsHandlers = new WSHandle[]{wsLogger};
        WSUtil wsUtil = new WSUtil();
        wsUtil.executeWS(this, wsHandlers);
        return this.getResult();
    }

    @Override
    public String toString() {
        try {
            return new com.fasterxml.jackson.databind.ObjectMapper()
                    .writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
