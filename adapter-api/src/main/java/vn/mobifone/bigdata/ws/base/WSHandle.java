package vn.mobifone.bigdata.ws.base;

public interface WSHandle <T extends OkHttpService>{
    void afterInitCommand(T command);
    void afterExecute(T command);
    void afterException(T command);
}
