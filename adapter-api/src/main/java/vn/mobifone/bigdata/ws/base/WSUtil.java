package vn.mobifone.bigdata.ws.base;

public class WSUtil extends BaseService{
    public <T extends OkHttpService> void executeWS(T service, WSHandle<T>[] wsHandles) {
        try {
            service.init();
            if (wsHandles != null) {
                for (WSHandle<T> wsHandler : wsHandles) {
                    wsHandler.afterInitCommand(service);
                }
            }
            service.setResult(service.execute());
            if (wsHandles != null) {
                for (WSHandle<T> wsHandler : wsHandles) {
                    wsHandler.afterExecute(service);
                }
            }
        } catch (Exception e) {
            service.setEx(e);
            if (wsHandles != null) {
                for (WSHandle<T> wsHandler : wsHandles) {
                    wsHandler.afterException(service);
                }
            }
        }
    }
}
