package vn.mobifone.bigdata.ws.base;

import com.fasterxml.jackson.core.JsonProcessingException;
import okhttp3.*;
import vn.mobifone.bigdata.en.HttpMethod;

import java.io.IOException;
import java.util.Map;

public abstract class OkHttpService <T> {

    protected OkHttpClient client;
    protected MediaType mediaType;
    protected String url;
    protected String jsonBody;
    protected HttpMethod httpMethod;
    protected Map<String, String> headerParams;
    private T result;
    private Exception ex;

    public abstract T execute() throws IOException ;

    public abstract void init();

    public OkHttpClient getClient() {
        return client;
    }

    public void setClient(OkHttpClient client) {
        this.client = client;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getJsonBody() {
        return jsonBody;
    }

    public void setJsonBody(String jsonBody) {
        this.jsonBody = jsonBody;
    }

    public Map<String, String> getHeaderParams() {
        return headerParams;
    }

    public void setHeaderParams(Map<String, String> headerParams) {
        this.headerParams = headerParams;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public Exception getEx() {
        return ex;
    }

    public void setEx(Exception ex) {
        this.ex = ex;
    }

    @Override
    public String toString() {
        try {
            return new com.fasterxml.jackson.databind.ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }
}
