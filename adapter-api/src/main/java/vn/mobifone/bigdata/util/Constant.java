package vn.mobifone.bigdata.util;

public class Constant {
    /**
     * redis key
     * */
    public static final String REDIS_KEY = "KEY";
    public static final String REDIS_ID = "ID";
}
