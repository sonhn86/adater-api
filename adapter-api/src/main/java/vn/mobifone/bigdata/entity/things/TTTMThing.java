package vn.mobifone.bigdata.entity.things;
/**
 * TTTM thing
 * */

import com.fasterxml.jackson.core.JsonProcessingException;

public class TTTMThing extends MainfluxThing {

    private String mcuId;
    private String name;
    private String vCode;
    private String address;
    private String latitude;
    private String longitude;
    private String telNumber;    
    private String simSerial;
    private String simNumber;
    private String volume;
    private String ip;
    private String firmwareVersion;
    private String fmVolume;
    private String fmAuto;
    private String txType;
    private String groupIdList;
    private String cfgCameraList;
    private String cfgSensorList;
    private String cfgEventList;    
    private String status;
    private String activeDate;
    private String deactivateDate;
    private String createdDate;

    public String getMcuId() {
        return mcuId;
    }

    public void setMcuId(String mcuId) {
        this.mcuId = mcuId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getvCode() {
        return vCode;
    }

    public void setvCode(String vCode) {
        this.vCode = vCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getSimSerial() {
        return simSerial;
    }

    public void setSimSerial(String simSerial) {
        this.simSerial = simSerial;
    }

    public String getSimNumber() {
        return simNumber;
    }

    public void setSimNumber(String simNumber) {
        this.simNumber = simNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getActiveDate() {
        return activeDate;
    }

    public void setActiveDate(String activeDate) {
        this.activeDate = activeDate;
    }

    public String getDeactivateDate() {
        return deactivateDate;
    }

    public void setDeactivateDate(String deactivateDate) {
        this.deactivateDate = deactivateDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
    
    public String getTelNumber() {
		return telNumber;
	}

	public void setTelNumber(String telNumber) {
		this.telNumber = telNumber;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getFirmwareVersion() {
		return firmwareVersion;
	}

	public void setFirmwareVersion(String firmwareVersion) {
		this.firmwareVersion = firmwareVersion;
	}

	public String getFmVolume() {
		return fmVolume;
	}

	public void setFmVolume(String fmVolume) {
		this.fmVolume = fmVolume;
	}

	public String getFmAuto() {
		return fmAuto;
	}

	public void setFmAuto(String fmAuto) {
		this.fmAuto = fmAuto;
	}

	public String getTxType() {
		return txType;
	}

	public void setTxType(String txType) {
		this.txType = txType;
	}

	public String getGroupIdList() {
		return groupIdList;
	}

	public void setGroupIdList(String groupIdList) {
		this.groupIdList = groupIdList;
	}

	public String getCfgCameraList() {
		return cfgCameraList;
	}

	public void setCfgCameraList(String cfgCameraList) {
		this.cfgCameraList = cfgCameraList;
	}

	public String getCfgSensorList() {
		return cfgSensorList;
	}

	public void setCfgSensorList(String cfgSensorList) {
		this.cfgSensorList = cfgSensorList;
	}

	public String getCfgEventList() {
		return cfgEventList;
	}

	public void setCfgEventList(String cfgEventList) {
		this.cfgEventList = cfgEventList;
	}

	@Override
    public String toString() {
        try {
            return new com.fasterxml.jackson.databind.ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }

}
