package vn.mobifone.bigdata.exception;

import vn.mobifone.bigdata.en.AuthExceptionType;
import vn.mobifone.bigdata.en.McuExceptionType;
import vn.mobifone.bigdata.en.ThingExceptionType;
import vn.mobifone.bigdata.entity.respone.ApiResponse;
import vn.mobifone.bigdata.util.RespUtil;

public class McuException extends Exception implements ApiException{

    private McuExceptionType mcuExceptionType;

    public McuException() {
        super();
    }

    public McuException(String message) {
        super(message);
    }

    public McuException(Throwable throwable) {
        super(throwable);
    }

    public McuException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public McuException(McuExceptionType type){
        super(type.getValue());
        mcuExceptionType = type;
    }


    public ApiResponse getMessageFromType(){
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setStatus(RespUtil.RESPONSE_CODE_NOK);
        switch (mcuExceptionType){
            case MCU_NOT_FOUND:
                apiResponse.setCode(RespUtil.GRANT_PERMISSION_NOT_FOUND_CODE);
                apiResponse.setMessage(RespUtil.GRANT_PERMISSION_NOT_FOUND_MESSAGE);
                break;
            case MCU_NOT_EXIT:
                apiResponse.setCode(RespUtil.THING_MAPPED_NOT_FOUND_CODE);
                apiResponse.setMessage(RespUtil.THING_MAPPED_NOT_FOUND_MESSAGE);
                break;
            case CANNOT_DELETE_PERMISSION_CHANNEL:
                apiResponse.setCode(RespUtil.MAINFLUX_THING_CHANNEL_CODE);
                apiResponse.setMessage(RespUtil.MAINFLUX_THING_CHANNEL_MESSAGE);
                break;
            case MCU_EXITED:
                apiResponse.setCode(RespUtil.THING_MAPPED_CODE);
                apiResponse.setMessage(RespUtil.THING_MAPPED_MESSAGE);
                break;
            case MCU_ID_NULL:
                apiResponse.setCode(RespUtil.MCU_ID_NULL_CODE);
                apiResponse.setMessage(RespUtil.MCU_ID_NULL_MESSAGE);
                break;
            case UNKNOWN:
                apiResponse.setCode(RespUtil.EXCEPTION_CODE);
                apiResponse.setMessage(RespUtil.EXCEPTION_MESSAGE);
                break;
			case CANNOT_SET_GROUP:
				apiResponse.setCode(RespUtil.GRANT_PERMISSION_ERROR_CODE);
                apiResponse.setMessage(RespUtil.GRANT_PERMISSION_ERROR_MESSAGE);
				break;
			default:
				apiResponse.setCode(RespUtil.EXCEPTION_CODE);
	            apiResponse.setMessage(RespUtil.EXCEPTION_MESSAGE);
				break;
        }
        return apiResponse;
    }
}
