package vn.mobifone.bigdata.repository.things.thingsrepo;

import vn.mobifone.bigdata.entity.respone.ApiResponse;
import vn.mobifone.bigdata.entity.respone.DBResponse;
import vn.mobifone.bigdata.entity.things.TTTMThing;
import vn.mobifone.bigdata.repository.things.thingsmodel.Things;

import java.util.List;

public interface ThingsRepositoryCustom {
    public List<Things> getAll() throws Exception;
}
