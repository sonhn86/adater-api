package vn.mobifone.bigdata.repository.tttm.tttmrepo;

import vn.mobifone.bigdata.entity.respone.ApiResponse;
import vn.mobifone.bigdata.entity.respone.DBResponse;
import vn.mobifone.bigdata.entity.things.TTTMThing;

public interface McuRepositoryCustom {
    public DBResponse mcuMapperInsert(TTTMThing tttmThing) throws Exception;
    public DBResponse mcuMapperRemove(TTTMThing tttmThing) throws Exception;
    public ApiResponse mcuCollectLog(TTTMThing tttmThing) throws Exception;
}
