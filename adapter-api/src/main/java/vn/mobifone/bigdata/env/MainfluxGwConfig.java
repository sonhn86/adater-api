package vn.mobifone.bigdata.env;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "mainflux-gw")
public class MainfluxGwConfig {

    private String sysUser;
    private String sysPassword;
    private String thingConnnectOnlyId;
    private String thingsConnectOnlyKey;
    private String sysChannelId;
    private String userEndpoint;
    private String thingEndpoint;
    private String thingIdEndpoint;
    private String getUserToken;
    private String createThing;
    private String getThingId;
    private String removeThing;
    private String channel;

    public String getSysUser() {
        return sysUser;
    }

    public void setSysUser(String sysUser) {
        this.sysUser = sysUser;
    }

    public String getSysPassword() {
        return sysPassword;
    }

    public void setSysPassword(String sysPassword) {
        this.sysPassword = sysPassword;
    }

    public String getThingConnnectOnlyId() {
        return thingConnnectOnlyId;
    }

    public void setThingConnnectOnlyId(String thingConnnectOnlyId) {
        this.thingConnnectOnlyId = thingConnnectOnlyId;
    }

    public String getThingsConnectOnlyKey() {
        return thingsConnectOnlyKey;
    }

    public void setThingsConnectOnlyKey(String thingsConnectOnlyKey) {
        this.thingsConnectOnlyKey = thingsConnectOnlyKey;
    }

    public String getSysChannelId() {
        return sysChannelId;
    }

    public void setSysChannelId(String sysChannelId) {
        this.sysChannelId = sysChannelId;
    }

    public String getUserEndpoint() {
        return userEndpoint;
    }

    public void setUserEndpoint(String userEndpoint) {
        this.userEndpoint = userEndpoint;
    }

    public String getThingEndpoint() {
        return thingEndpoint;
    }

    public void setThingEndpoint(String thingEndpoint) {
        this.thingEndpoint = thingEndpoint;
    }

    public String getGetUserToken() {
        return getUserToken;
    }

    public void setGetUserToken(String getUserToken) {
        this.getUserToken = getUserToken;
    }

    public String getCreateThing() {
        return createThing;
    }

    public void setCreateThing(String createThing) {
        this.createThing = createThing;
    }

    public String getGetThingId() {
        return getThingId;
    }

    public void setGetThingId(String getThingId) {
        this.getThingId = getThingId;
    }

    public String getRemoveThing() {
        return removeThing;
    }

    public void setRemoveThing(String removeThing) {
        this.removeThing = removeThing;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getThingIdEndpoint() {
		return thingIdEndpoint;
	}

	public void setThingIdEndpoint(String thingIdEndpoint) {
		this.thingIdEndpoint = thingIdEndpoint;
	}

	@Override
    public String toString() {
        try {
            return new com.fasterxml.jackson.databind.ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }
}
