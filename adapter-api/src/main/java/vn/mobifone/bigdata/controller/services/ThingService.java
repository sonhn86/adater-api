package vn.mobifone.bigdata.controller.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import vn.mobifone.bigdata.en.AuthExceptionType;
import vn.mobifone.bigdata.en.McuExceptionType;
import vn.mobifone.bigdata.en.ThingExceptionType;
import vn.mobifone.bigdata.entity.respone.ApiResponse;
import vn.mobifone.bigdata.entity.respone.DBResponse;
import vn.mobifone.bigdata.entity.respone.mainflux.MainfluxResponse;
import vn.mobifone.bigdata.entity.things.TTTMThing;
import vn.mobifone.bigdata.env.MainfluxGwConfig;
import vn.mobifone.bigdata.exception.AuthException;
import vn.mobifone.bigdata.exception.McuException;
import vn.mobifone.bigdata.exception.ThingException;
import vn.mobifone.bigdata.repository.tttm.tttmrepo.McuRepositoryCustom;
import vn.mobifone.bigdata.service.MainfluxGwService;
import vn.mobifone.bigdata.service.RedisService;
import vn.mobifone.bigdata.util.Constant;
import vn.mobifone.bigdata.util.RespUtil;
import vn.mobifone.bigdata.util.UuidUtil;

public class ThingService {
	private static final Logger logger = LogManager.getLogger(ThingService.class);

	protected RedisService redisService;

	protected MainfluxGwService mainfluxGwService;

	protected McuRepositoryCustom mcuRepositoryCustom;

	protected MainfluxGwConfig mainfluxGwConfig;

	protected String sysChannelId;

	public ThingService(RedisService redisService, MainfluxGwService mainfluxGwService,
			McuRepositoryCustom mcuRepositoryCustom, MainfluxGwConfig mainfluxGwConfig) {
		this.redisService = redisService;
		this.mainfluxGwService = mainfluxGwService;
		this.mcuRepositoryCustom = mcuRepositoryCustom;
		this.mainfluxGwService = mainfluxGwService;
		this.sysChannelId = mainfluxGwConfig.getSysChannelId();
	}

	/**
	 * verify mcu
	 */

	public ApiResponse verifyMcuV1(TTTMThing tttmThing) {
		ApiResponse apiResponse = new ApiResponse();
		try {
			if (tttmThing.getMcuId().isEmpty()) {
				throw new McuException(McuExceptionType.MCU_ID_NULL);
			} else {
				if (isMapped(tttmThing.getMcuId() == null ? "" : tttmThing.getMcuId())) {
					throw new McuException(McuExceptionType.MCU_EXITED);
				} else {
					MainfluxResponse mainfluxResponse = mainfluxGwService.token();
					if (mainfluxResponse != null && mainfluxResponse.getCode() == RespUtil.MF_CREATE_SUCCESS_CODE) {
						String key = UuidUtil.generateType1UUID();
						String name = tttmThing.getName() == null ? tttmThing.getMcuId() : tttmThing.getName();
						MainfluxResponse mainfluxResponseCreateThing = mainfluxGwService.createThing(key, name,
								new Object(), mainfluxResponse.getToken());
						if (mainfluxResponseCreateThing != null
								&& mainfluxResponseCreateThing.getCode() == RespUtil.MF_CREATE_SUCCESS_CODE) {
							MainfluxResponse mainfluxResponseGetThingId = mainfluxGwService.getThingId(key,
									mainfluxResponse.getToken());
							if (mainfluxResponseGetThingId.getCode() == RespUtil.MF_GET_SUCCESS_CODE) {
								String mfThingId = mainfluxResponseGetThingId.getId();
								if (mfThingId != null && !mfThingId.equals("")) {
									// insert database
									tttmThing.setThingId(mfThingId);
									tttmThing.setThingKey(key);
									DBResponse dbResponse = mcuRepositoryCustom.mcuMapperInsert(tttmThing);
									// insert redis
									if (dbResponse.getErrorId().equals(RespUtil.DATABASE_STATUS_SUCCESS)) {
										redisService.setHValue(tttmThing.getMcuId(), Constant.REDIS_ID,
												tttmThing.getThingId());
										redisService.setHValue(tttmThing.getMcuId(), Constant.REDIS_KEY,
												tttmThing.getThingKey());
										apiResponse.setStatus(RespUtil.RESPONSE_CODE_OK);
										apiResponse.setCode(RespUtil.THING_VERIFY_SUCCESS_CODE);
										apiResponse.setMessage(RespUtil.THING_VERIFY_SUCCESS_MESSAGE);
									} else {
										throw new ThingException(ThingExceptionType.CANNOT_MAP_MCU_THING);
									}
								} else {
									throw new ThingException(ThingExceptionType.NO_THING_INFO);
								}
							} else {
								throw new ThingException(ThingExceptionType.NO_THING_INFO);
							}

						} else {
							throw new ThingException(ThingExceptionType.CANNOT_CREATE_THING);
						}
					} else {
						throw new AuthException(AuthExceptionType.GET_TOKEN_ERROR);
					}
				}
			}
		} catch (McuException | ThingException | AuthException e) {
			logger.error(e.getMessage());
			apiResponse = e.getMessageFromType();
		} catch (Exception e) {
			logger.error(e);
			apiResponse.setStatus(RespUtil.RESPONSE_CODE_NOK);
			apiResponse.setCode(RespUtil.EXCEPTION_CODE);
			apiResponse.setMessage(RespUtil.EXCEPTION_MESSAGE);
		}
		return apiResponse;
	}

	/**
	 * delete mcu
	 */

	public ApiResponse deleteMcuV1(TTTMThing tttmThing) {
		ApiResponse apiResponse = new ApiResponse();
		try {
			if (tttmThing.getMcuId().isEmpty()) {
				throw new McuException(McuExceptionType.MCU_ID_NULL);
			} else {
				if (!isMapped(tttmThing.getMcuId() == null ? "" : tttmThing.getMcuId())) {
					throw new McuException(McuExceptionType.MCU_NOT_EXIT);
				} else {
					tttmThing.setThingId(redisService.getValue(tttmThing.getMcuId(), Constant.REDIS_ID) == null ? null
							: redisService.getValue(tttmThing.getMcuId(), Constant.REDIS_ID).toString());
					tttmThing.setThingKey(redisService.getValue(tttmThing.getMcuId(), Constant.REDIS_KEY) == null ? null
							: redisService.getValue(tttmThing.getMcuId(), Constant.REDIS_KEY).toString());
					DBResponse dbResponse = mcuRepositoryCustom.mcuMapperRemove(tttmThing);
					if (dbResponse.getErrorId().equals(RespUtil.DATABASE_STATUS_SUCCESS)) {
						MainfluxResponse mainfluxResponse = mainfluxGwService.token();
						if (mainfluxResponse != null && mainfluxResponse.getCode() == RespUtil.MF_CREATE_SUCCESS_CODE) {
							MainfluxResponse mainfluxResponseDeleteThingChannelPermission = mainfluxGwService
									.deleteThingChannelPermission(mainfluxResponse.getToken(), tttmThing.getThingId(),
											sysChannelId);
							// clear redis
							redisService.removeKey(tttmThing.getMcuId());
							apiResponse.setStatus(RespUtil.RESPONSE_CODE_OK);
							apiResponse.setCode(RespUtil.MCU_DELETE_SUCCESS_CODE);
							apiResponse.setMessage(RespUtil.MCU_DELETE_SUCCESS_MESSAGE);
						} else {
							throw new AuthException(AuthExceptionType.GET_TOKEN_ERROR);
						}
					} else {
						throw new ThingException(ThingExceptionType.CANNOT_UPDATE_THING_STATUS);
					}
				}
			}
		} catch (ThingException | McuException | AuthException e) {
			logger.error(e.getMessage());
			apiResponse = e.getMessageFromType();
		} catch (Exception e) {
			logger.error(e);
			apiResponse.setStatus(RespUtil.RESPONSE_CODE_NOK);
			apiResponse.setCode(RespUtil.EXCEPTION_CODE);
			apiResponse.setMessage(RespUtil.EXCEPTION_MESSAGE);
		}
		return apiResponse;
	}

	/**
	 * set group mcu
	 */

	public ApiResponse setGroupMcuV1(TTTMThing tttmThing) {
		ApiResponse apiResponse = new ApiResponse();
		try {
			if (tttmThing.getMcuId().isEmpty()) {
				throw new McuException(McuExceptionType.MCU_ID_NULL);
			} else {
				if (!isMapped(tttmThing.getMcuId() == null ? "" : tttmThing.getMcuId())) {
					throw new McuException(McuExceptionType.MCU_NOT_FOUND);
				} else {

					tttmThing.setThingId(redisService.getValue(tttmThing.getMcuId(), Constant.REDIS_ID) == null ? null
							: redisService.getValue(tttmThing.getMcuId(), Constant.REDIS_ID).toString());
					tttmThing.setThingKey(redisService.getValue(tttmThing.getMcuId(), Constant.REDIS_KEY) == null ? null
							: redisService.getValue(tttmThing.getMcuId(), Constant.REDIS_KEY).toString());

					MainfluxResponse mainfluxResponse = mainfluxGwService.token();

					MainfluxResponse mainfluxResponseGrantThingChannelPermission = mainfluxGwService
							.grantThingChannelPermission(mainfluxResponse.getToken(), tttmThing.getThingId(),
									sysChannelId);
					if (mainfluxResponseGrantThingChannelPermission
							.getCode() == RespUtil.MF_GRANT_PERMISSION_SUCCESS_CODE) {
						apiResponse.setStatus(RespUtil.RESPONSE_CODE_OK);
						apiResponse.setCode(RespUtil.GRANT_PERMISSION_SUCCESS_CODE);
						apiResponse.setMessage(RespUtil.GRANT_PERMISSION_SUCCESS_MESSAGE);
					} else {
						throw new McuException(McuExceptionType.CANNOT_SET_GROUP);
					}
				}
			}
		} catch (McuException e) {
			logger.error(e.getMessage());
			apiResponse = e.getMessageFromType();
		} catch (Exception e) {
			logger.error(e);
			apiResponse.setStatus(RespUtil.RESPONSE_CODE_NOK);
			apiResponse.setCode(RespUtil.EXCEPTION_CODE);
			apiResponse.setMessage(RespUtil.EXCEPTION_MESSAGE);
		}
		return apiResponse;
	}

	/**
	 * collect log
	 */

	public ApiResponse collectLogV1(TTTMThing tttmThing) {
		ApiResponse apiResponse = new ApiResponse();
		try {
			if (tttmThing.getMcuId().isEmpty()) {
				throw new McuException(McuExceptionType.MCU_ID_NULL);
			} else {
				apiResponse = mcuRepositoryCustom.mcuCollectLog(tttmThing);
			}
		} catch (McuException e) {
			logger.error(e.getMessage());
			apiResponse = e.getMessageFromType();
		} catch (Exception e) {
			logger.error(e);
			apiResponse.setStatus(RespUtil.RESPONSE_CODE_NOK);
			apiResponse.setCode(RespUtil.EXCEPTION_CODE);
			apiResponse.setMessage(RespUtil.EXCEPTION_MESSAGE);
		}
		return apiResponse;
	}

	/**
	 * protected function
	 */

	protected boolean isMapped(String mcuID) {
		boolean check = false;
		Object id_check = redisService.getValue(mcuID, Constant.REDIS_ID);
		if (id_check != null) {
			check = true;
		}
		return check;
	}

}
